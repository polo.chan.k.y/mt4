//+------------------------------------------------------------------+
//|                                                  H4_Strategy.mq4 |
//|                                        Copyright 2020, Polo Chan |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
//version 7.0
#property strict

#define MAGICNBR  20200501

#define INDEX   uint

int      trand = 0;  // 1:up, 2:down
int      trand_count = 0; //計算趨勢轉向次數
double   down = 0;
double   top = 0;
int      hold = 0;
int      c_ind = 0; //是否已 init val ( 0:N, 1:Y )

double   lowest = 0;
double   highest = 0;
int      tmp_int = 0;
double   tp = 0;
double   cl = 0;
int      setStopInd = 0;

int      abc_t = 0;
input double lot = 1; //手數
input int max_loss = 3000; //止蝕大於 N 唔做

long indexChartId = 0;

enum abc_type 
  {
   abc_A=0,  //否
   abc_B=1   //是
  };


enum in_m_type 
  {
   in_A=0,  //等收燭
   in_B=1   //Stop 單
  };

enum tp_m_type 
  {
   tp_A=0,  // 多於 N 步
   tp_B=1   // 止賺:止蝕 8:10 
  };

input in_m_type in_m=in_B; //入市方法
input in_m_type out_m=in_B; //出市方法

input tp_m_type tp_m=tp_A; //止賺方法
input double fix_tp = 200; //└止賺(多於 N 步)
input double fix_tp_percent = 80; //└止賺%對比止蝕(止賺:止蝕 8:10)

input abc_type abc_ind = abc_A; //是否加入ABC單
input double a_lot = 2; //└B單手數
input double a_percent = 100; //└B單(賺回A單多少％)
input double b_lot = 4; //└C單手數
input double b_percent = 100; //└C單(賺回B單多少％)

input double input1 = 30;
input double input2 = 2;


string cmt;
void OnStart() {
   Print("Start");
   getPIP("EURUSD");
   getPIP("XAUUSD");
   getPIP(".DE30Cash");
}

double getPIP(string sym){

      int       dig  =  (int)MarketInfo(sym,MODE_DIGITS);
      double 
                loS  =  MarketInfo(sym,MODE_LOTSIZE), // "LotSize",
                tiS  =  MarketInfo(sym,MODE_TICKSIZE),
                hmm  =  ((int)MathLog10(loS)-1),
                pip  =  MathPow(10.0, (-hmm));
         
      cmt = cmt+"\n\n"+sym+"  Digits: "+(string)dig+"  TickValue (Acct.-Curr): "+DoubleToStr(MarketInfo(sym,MODE_TICKVALUE),3)+
                         "  TickSize: "+DoubleToStr(tiS,dig)+"  LotSize: "+DoubleToStr(loS,2)+" BaseCurr\n";
      cmt = cmt+" Log (=Digits): "+(string)hmm+"  PIP: "+DoubleToStr(pip,dig);
      Print(cmt);
      return(pip);

}

bool isClose(string orderType){      
   double profit_point = 0;
   if(abc_t == 0){
      if(tp_m == tp_A){ // 多於 N 步
         if( orderType == "BUY" ){
            profit_point = (OrderClosePrice() - OrderOpenPrice()) / Point();
            return profit_point >= fix_tp || down >= OrderClosePrice();
         } else if( orderType == "SELL" ){
            profit_point = (OrderOpenPrice() - OrderClosePrice()) / Point();
            return profit_point >= fix_tp || OrderClosePrice() >= top;
         }
      } else if(tp_m == tp_B){ //止賺:止蝕 8:10
         if( orderType == "BUY" ){
            profit_point = (OrderClosePrice() - OrderOpenPrice()) / Point();
            tp = ((OrderOpenPrice() - down) / Point()) * (fix_tp_percent / 100);
            return profit_point >= tp || down >= OrderClosePrice();
         } else if( orderType == "SELL" ){
            profit_point = (OrderOpenPrice() - OrderClosePrice()) / Point();
            tp = ((top - OrderOpenPrice()) / Point()) * (fix_tp_percent / 100);
            return profit_point >= tp || OrderClosePrice() >= top;
         }
      }
   } else {
      
      return OrderClosePrice() <= down || OrderClosePrice() >= top;
   }
   
   return false;
}

void processCloseOrder(){
   for(int i=(OrdersTotal()-1);i>=0;i--){
      if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES)==false) break;
      if(OrderMagicNumber()!=MAGICNBR || OrderSymbol()!=Symbol()) continue;
      
      if(OrderType()==OP_BUYLIMIT || OrderType()==OP_BUYSTOP || OrderType()==OP_SELLSTOP || OrderType()==OP_SELLLIMIT){
         //If there was an error log it
         if(!OrderDelete(OrderTicket(),White)) Print("ERROR - Unable to close the order - ",OrderTicket()," - ",GetLastError());
      }
    
      if(OrderType()==OP_BUY && isClose("BUY")){
         
        hold = -2;
         
        if(!OrderClose(OrderTicket(),OrderLots(),OrderClosePrice(),3,White))
            Print("OrderClose error ",GetLastError());
            
        if(abc_ind > 0){    
           
           bool r_ind = (OrderClosePrice() - OrderOpenPrice()) / Point() < 0;
               
           if(r_ind){
               if(abc_t == 0){
                  abc_t = 1;
               
                  int tmp_tp = NormalizeDouble((OrderOpenPrice() - OrderClosePrice()) * ( a_percent / 100 ) / a_lot / Point() ,0);
                  
                  top = highest;
                  
                  down = Bid-tmp_tp*Point();
                  
                  int ticket=OrderSend(Symbol(),OP_SELL,a_lot,Bid,3,top,down,"A單");
      
                  if(ticket<0){
                     Print("OrderSend failed with error 6#",GetLastError());
                  }   
               } else if(abc_t == 1){
               
               } else if(abc_t == 2){
               
               }
           }
        }
         
      } else if(OrderType()==OP_SELL && isClose("SELL")){
         
         hold = -2;
         
         if(!OrderClose(OrderTicket(),OrderLots(),OrderClosePrice(),3,White))
            Print("OrderClose error ",GetLastError());
         
         if(abc_ind > 0){
            bool r_ind = (OrderOpenPrice() - OrderClosePrice()) / Point() < 0;
             
            if(r_ind){
               Print("abc_t2 : " + abc_t);
               if(abc_t == 0){
                  abc_t = 1;
               
                  int tmp_tp = NormalizeDouble((OrderClosePrice() - OrderOpenPrice()) * ( a_percent / 100 ) / a_lot / Point() ,0);
                  
                  top = Ask+tmp_tp*Point();
                  
                  down = lowest;
                   
                  int ticket=OrderSend(Symbol(),OP_BUY,a_lot,Ask,3,down,top,"A單");
      
                  if(ticket<0){
                     Print("OrderSend failed with error 6#",GetLastError());
                  }   
               } else if(abc_t == 1){
               
               } else if(abc_t == 2){
               
               }
            }
         }
      }
   }
}

bool isExistsOrder(){
   int count_order = 0;
   for(int i=(OrdersTotal()-1);i>=0;i--){
      if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES)==false) break;
      if(OrderMagicNumber()!=MAGICNBR || OrderSymbol()!=Symbol()) continue;
      if(OrderType()==OP_BUY || OrderType()==OP_SELL){
         count_order++;
         break;
      }
   }
   
   return count_order > 0;
}

void outMatket(){
   if(out_m == in_A){ // 收燭先計
      if(!isNewBar()) return;
   } 
   
   if(isExistsOrder()){
      processCloseOrder();
   }
}

void OnTick(){

   // if(!(Hour() > 8 && Hour() < 14)){
   //    return;
   // }

   /*
   //check start candle
   if( currentBarIndex() == 0 && isNewBar() ){
      //init val
      startnInitPara();
      
   } else if (c_ind == 1) {
      if(trand_count <= 2){
         //計算支持阻力, 高低位
         processPrice();
      } else {
         if(hold == 0 && setStopInd == 0){
            //入市
            inMarket();
         } else {
            //出市
            outMatket();
         }
      }
   }
   */
   int orderAInd;
   int orderBInd;
   int orderCInd;
   int orderDInd;
   int orderEInd;
   int orderFInd;

   int orderAType;
   int orderBType;
   int orderCType;
   int orderDType;
   int orderEType;
   int orderFType;

   for(int i=0; i < OrdersTotal(); i++){
      if(OrderSelect(i, SELECT_BY_POS, MODE_TRADES) == true){
         string tmp = OrderComment();
         if(tmp == "A"){ orderAInd = 1; orderAType = OrderType(); } 
         else if(tmp == "B"){ orderBInd = 1; orderBType = OrderType();}
         else if(tmp == "C"){ orderCInd = 1; orderCType = OrderType();}
         else if(tmp == "D"){ orderDInd = 1; orderDType = OrderType();}
         else if(tmp == "E"){ orderEInd = 1; orderEType = OrderType();}
         else if(tmp == "F"){ orderFInd = 1; orderFType = OrderType();}
      }
   }

   double bbLow = iBands(NULL,0,7,1.8,0,PRICE_CLOSE,2,1);
   double bbHigh = iBands(NULL,0,7,1.8,0,PRICE_CLOSE,1,1);

   // if(OrdersTotal() == 2 && false){
   //    if(OrderSelect(0,SELECT_BY_POS,MODE_TRADES)==false){
   //       Print("Access to history failed with error (",GetLastError(),")");
   //    }

   //    if(OrderType() == OP_BUY){
   //       if(OrderOpenPrice() < NormalizeDouble(bbHigh,Digits) && OrderTakeProfit() != NormalizeDouble(bbHigh,Digits)){
   //          Print("OP_BUY OrderTicket() : #", OrderTicket());
   //          Print("OrderOpenPrice() : ", OrderOpenPrice());
   //          Print("OrderStopLoss() : ", OrderStopLoss());
   //          Print("OrderTakeProfit() : ", OrderTakeProfit());
   //          Print("bbHigh New Profit() :",NormalizeDouble(bbHigh,Digits));
   //          bool res=OrderModify(OrderTicket(),OrderOpenPrice(),OrderStopLoss(),NormalizeDouble(bbHigh,Digits),0,Blue);
   //          if(!res)
   //             Print("Error in OrderModify. Error code=",GetLastError());
   //          else
   //             Print("Order modified successfully.");
   //       }
         
   //    } else if(OrderType() == OP_SELL){
   //       if(OrderOpenPrice() > NormalizeDouble(bbLow,Digits) && OrderTakeProfit() != NormalizeDouble(bbLow,Digits)){
   //          Print("OP_SELL OrderTicket() : ", OrderTicket());
   //          Print("OrderOpenPrice() : ", OrderOpenPrice());
   //          Print("OrderStopLoss() : ", OrderStopLoss());
   //          Print("OrderTakeProfit() : ", OrderTakeProfit());
   //          Print("bbLow New Profit() :",NormalizeDouble(bbLow,Digits));
   //          bool res=OrderModify(OrderTicket(),OrderOpenPrice(),OrderStopLoss(),NormalizeDouble(bbLow,Digits),0,Blue);
   //          if(!res)
   //             Print("Error in OrderModify. Error code=",GetLastError());
   //          else
   //             Print("Order modified successfully.");
   //       }
   //    }
   // }
   
   if(OrdersTotal() == 1){

      if((orderBInd && orderBType > 1) || (orderCInd && orderCType > 1) || (orderDInd && orderDType > 1) || (orderEInd && orderEType > 1) ||  (orderFInd && orderFType > 1)){
         Print("cancelAllOrder");
         cancelAllOrder();
      } else {
         

         if(orderBInd && orderBType < 2){
            if(OrderSelect(0,SELECT_BY_POS,MODE_TRADES)==false){
               Print("Access to history failed with error (",GetLastError(),")");
            }

            if(OrderType() == OP_BUY){
               double takePro = bbLow;
               //double takePro = NormalizeDouble(OrderStopLoss() + ((OrderStopLoss() - OrderOpenPrice()) / input2),Digits);
               int ticket3=OrderSend(Symbol(),OP_SELLSTOP,OrderLots()*2,OrderStopLoss(),3,OrderOpenPrice(),takePro,"C",MAGICNBR);   
               if(ticket3<0){
                  Print("OrderSend failed with error 1#",GetLastError());
               }
            } else {
               double takePro = bbHigh;
               //double takePro = NormalizeDouble(OrderStopLoss() - ((OrderOpenPrice() - OrderStopLoss()) / input2),Digits);
               int ticket3=OrderSend(Symbol(),OP_BUYSTOP,OrderLots()*2,OrderStopLoss(),3,OrderOpenPrice(),takePro,"C",MAGICNBR);
               if(ticket3<0){
                  Print("OrderSend failed with error 1#",GetLastError());
               }
            }
         }

         if(orderCInd && orderCType < 2){
            if(OrderSelect(0,SELECT_BY_POS,MODE_TRADES)==false){
               Print("Access to history failed with error (",GetLastError(),")");
            }

            if(OrderType() == OP_BUY){
               double takePro = NormalizeDouble(OrderStopLoss() + ((OrderStopLoss() - OrderOpenPrice()) / input2),Digits);
               int ticket3=OrderSend(Symbol(),OP_SELLSTOP,OrderLots()*2,OrderStopLoss(),3,OrderOpenPrice(),takePro,"D",MAGICNBR);   
               if(ticket3<0){
                  Print("OrderSend failed with error 1#",GetLastError());
               }
            } else {
               double takePro = NormalizeDouble(OrderStopLoss() - ((OrderOpenPrice() - OrderStopLoss()) / input2),Digits);
               int ticket3=OrderSend(Symbol(),OP_BUYSTOP,OrderLots()*2,OrderStopLoss(),3,OrderOpenPrice(),takePro,"D",MAGICNBR);
               if(ticket3<0){
                  Print("OrderSend failed with error 1#",GetLastError());
               }
            }
         }

         if(orderDInd && orderDType < 2){
            if(OrderSelect(0,SELECT_BY_POS,MODE_TRADES)==false){
               Print("Access to history failed with error (",GetLastError(),")");
            }

            if(OrderType() == OP_BUY){
               //double takePro = NormalizeDouble(OrderStopLoss() + ((OrderStopLoss() - OrderOpenPrice()) / 3),Digits);
               double takePro = bbLow;
               int ticket3=OrderSend(Symbol(),OP_SELLSTOP,OrderLots()*2,OrderStopLoss(),3,OrderOpenPrice(),takePro,"E",MAGICNBR);   
               if(ticket3<0){
                  Print("OrderSend failed with error 1#",GetLastError());
               }
            } else {
               //double takePro = NormalizeDouble(OrderStopLoss() - ((OrderOpenPrice() - OrderStopLoss()) / 3),Digits);
               double takePro = bbHigh;
               int ticket3=OrderSend(Symbol(),OP_BUYSTOP,OrderLots()*2,OrderStopLoss(),3,OrderOpenPrice(),takePro,"E",MAGICNBR);
               if(ticket3<0){
                  Print("OrderSend failed with error 1#",GetLastError());
               }
            }
         }

         if(orderEInd && orderEType < 2){
            if(OrderSelect(0,SELECT_BY_POS,MODE_TRADES)==false){
               Print("Access to history failed with error (",GetLastError(),")");
            }

            if(OrderType() == OP_BUY){
               double takePro = NormalizeDouble(OrderStopLoss() + ((OrderStopLoss() - OrderOpenPrice()) / input2),Digits);
               int ticket3=OrderSend(Symbol(),OP_SELLSTOP,OrderLots()*2,OrderStopLoss(),3,OrderOpenPrice(),takePro,"F",MAGICNBR);   
               if(ticket3<0){
                  Print("OrderSend failed with error 1#",GetLastError());
               }
            } else {
               double takePro = NormalizeDouble(OrderStopLoss() - ((OrderOpenPrice() - OrderStopLoss()) / input2),Digits);
               int ticket3=OrderSend(Symbol(),OP_BUYSTOP,OrderLots()*2,OrderStopLoss(),3,OrderOpenPrice(),takePro,"F",MAGICNBR);
               if(ticket3<0){
                  Print("OrderSend failed with error 1#",GetLastError());
               }
            }
         }
      }
   }

   

   //Print(orderAInd,orderBInd,orderCInd,orderDInd,orderEInd,orderFInd);



   // for(int i=0; i < OrdersTotal(); i++){
   //    if(OrderSelect(i, SELECT_BY_POS, MODE_TRADES) == true){
   //       // double bbLow = iBands(NULL,0,7,1.8,0,PRICE_CLOSE,2,1);
   //       // double bbHigh = iBands(NULL,0,7,1.8,0,PRICE_CLOSE,1,1);
         
   //       // if(OrderType() == OP_BUY && Close[0] > bbHigh){
   //       //    Print("OP_BUY Close : " + Close[0]);
   //       //    Print("Bid : " + Bid);
   //       //    Print("Ask : " + Ask);
   //       //    Print("bbHigh : " + bbHigh);
   //       //    Print("bbLow : " + bbLow);
   //       //    cancelAllOrder();
   //       // } else if(OrderType() == OP_SELL && bbLow > Close[0]){

   //       //    Print("OP_SELL Close : " + Close[0]);
   //       //    Print("Bid : " + Bid);
   //       //    Print("Ask : " + Ask);
   //       //    Print("bbHigh : " + bbHigh);
   //       //    Print("bbLow : " + bbLow);
   //       //    cancelAllOrder();
   //       // }
   //       if(OrderComment() == "B" && (OrderType() == OP_SELL || OrderType() == OP_BUY)){
   //          Print(OrderComment());
   //          Print("open price for the order 0 ",OrderOpenPrice());
   //          Print("open time for the order 0 ",OrderOpenTime());
   //          Print("Order #",OrderTicket()," profit: ", OrderTakeProfit());
   //          Print("Order Type: ",OrderType());
   //       }
   //    }
   // }

   if(isNewBar()){
   //    double bbLow = iBands(NULL,0,7,1.8,0,PRICE_CLOSE,2,1);
   //    double bbHigh = iBands(NULL,0,7,1.8,0,PRICE_CLOSE,1,1);
      
      if(Close[1] > bbHigh && Open[1] < bbHigh && High[1]-Close[1] > input1/MathPow(10, Digits())  ){
         // Print("High BB : " + bbHigh + " - " + bbLow);
         // Print("High[1]-Close[1] : " + (High[1]-Close[1]));
         // Print("input1/MathPow(10, Digits()) : " + input1/MathPow(10, Digits()));

         //int time=TimeCurrent();
         //double price=SymbolInfoDouble(Symbol(),SYMBOL_BID);

         //VLineCreate("_VLineCreate_" + TimeToString(Time[0]));

         //ArrowBuyCreate("_ArrowBuyCreate_" + TimeToString(Time[0]));

         // for(int i=(OrdersTotal()-1);i>=0;i--){
         //    if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES)==false) continue;
         //    if(OrderMagicNumber()!=MAGICNBR || OrderSymbol()!=Symbol()) continue; 

         //    Print("[" + i + "] OrderType() : " + OrderType());
         //    if(OrderType() == OP_BUY) cancelAllOrder();
         // }
         Print("-----------------------------------------------------------------------------");
         int ticket=OrderSend(Symbol(),OP_SELL,lot,Bid,3,High[1],bbLow,"A",MAGICNBR);
         if(ticket<0)
           {
            Print("OrderSend failed with error 1#",GetLastError());
           }

         double takePro = NormalizeDouble(High[1] + ((High[1] - Bid) / input2),Digits);
         int ticket2=OrderSend(Symbol(),OP_BUYSTOP,lot*2,High[1],3,Bid,takePro,"B",MAGICNBR);
         //int ticket2=OrderSend(Symbol(),OP_BUYSTOP,lot*2,High[1],3,Low[1],takePro,"B",MAGICNBR);
         if(ticket2<0)
           {
            Print("OrderSend failed with error 1#",GetLastError());
           }
      }

      if(Close[1] < bbLow && Open[1] > bbLow && Close[1]-Low[1] > input1/MathPow(10, Digits()) ){
         // Print("Low BB : " + bbHigh + " - " + bbLow);
         // Print("Close[1]-Low[1] : " + (Close[1]-Low[1]));
         // Print("input1/MathPow(10, Digits()) : " + input1/MathPow(10, Digits()));
         Print("-----------------------------------------------------------------------------");
         int ticket=OrderSend(Symbol(),OP_BUY,lot,Ask,3,Low[1],bbHigh,"A",MAGICNBR);
         if(ticket<0)
           {
            Print("OrderSend failed with error 1#",GetLastError());
           }

         double takePro = NormalizeDouble(Low[1] - ((Bid - Low[1]) / input2),Digits);
      
         int ticket2=OrderSend(Symbol(),OP_SELLSTOP,lot*2,Low[1],3,Bid,takePro,"B",MAGICNBR);
         //int ticket2=OrderSend(Symbol(),OP_SELLSTOP,lot*2,Low[1],3,High[1],takePro,"B",MAGICNBR);
         if(ticket2<0)
           {
            Print("OrderSend failed with error 1#",GetLastError());
           }
      }
      
   }
}

int currentBarIndex(){
   return iBarShift(Symbol(),PERIOD_CURRENT,iTime(Symbol(),PERIOD_W1,0)) - 1;
}

bool VLineCreate(string            chart_ID=0,        // chart's ID
                 const int             sub_window=0,      // subwindow index
                 datetime              time=0,            // line time
                 const color           clr=White,        // line color
                 const ENUM_LINE_STYLE style=STYLE_SOLID, // line style
                 const int             width=1,           // line width
                 const bool            back=false,        // in the background
                 const bool            selection=true,    // highlight to move
                 const bool            hidden=true,       // hidden in the object list
                 const long            z_order=-10){        // priority for mouse click
  
   //--- if the line time is not set, draw it via the last bar
   if(!time)
      time=TimeCurrent();
   //--- reset the error value
      ResetLastError();
   //--- create a vertical line
      
   string label = chart_ID;

   Print(StrToInteger(label));

   if(!ObjectCreate(StrToInteger(label), label,OBJ_VLINE,sub_window,time,0))
     {
      Print(__FUNCTION__,
            ": failed to create a vertical line! Error code = ",GetLastError());
      return(false);
     }

   ObjectSet(label, OBJPROP_COLOR, clr);

   ObjectSet(label,OBJPROP_ZORDER,z_order);

   return(true);
  }



bool ArrowBuyCreate(
                     string                chart_ID=0,        // chart's ID
                    datetime              time=0,            // anchor point time
                    double                price=0,           // anchor point price
                    
                    const string          name="ArrowBuy",   // sign name
                    const int             sub_window=0,      // subwindow index
                    const color           clr=C'3,95,172',   // sign color
                    const ENUM_LINE_STYLE style=STYLE_SOLID, // line style (when highlighted)
                    const int             width=1,           // line size (when highlighted)
                    const bool            back=false,        // in the background
                    const bool            selection=false,   // highlight to move
                    const bool            hidden=true,       // hidden in the object list
                    const long            z_order=0){        // priority for mouse click


   string label = chart_ID;

   if(!time)
      time=TimeCurrent();

      if(!price)
      price=High[0];

   //--- create the sign
   if(!ObjectCreate(StrToInteger(label), label,OBJ_ARROW_BUY,sub_window,time,price))
     {
      Print(__FUNCTION__,
            ": failed to create \"Buy\" sign! Error code = ",GetLastError());
      return(false);
     }

   //--- set a sign color
   ObjectSetInteger(StrToInteger(label), label,OBJPROP_COLOR,clr);
   //--- set a line style (when highlighted)
   ObjectSetInteger(StrToInteger(label), label,OBJPROP_STYLE,style);
   //--- set a line size (when highlighted)
   ObjectSetInteger(StrToInteger(label), label,OBJPROP_WIDTH,width);
   //--- display in the foreground (false) or background (true)
   ObjectSetInteger(StrToInteger(label), label,OBJPROP_BACK,back);
   //--- enable (true) or disable (false) the mode of moving the sign by mouse
   ObjectSetInteger(StrToInteger(label), label,OBJPROP_SELECTABLE,selection);
   ObjectSetInteger(StrToInteger(label), label,OBJPROP_SELECTED,selection);
   //--- hide (true) or display (false) graphical object chart_ID in the object list
   ObjectSetInteger(StrToInteger(label), label,OBJPROP_HIDDEN,hidden);
   //--- set the priority for receiving the event of a mouse click in the chart
   ObjectSetInteger(StrToInteger(label), label,OBJPROP_ZORDER,z_order);
   //--- successful execution

   return(true);
}

void Trend_Line(
    string id,
    datetime x1, datetime x2, double y1, 
    double y2, color lineColor, double style){
    //~~~~~~~~~~s
    string label = id;
    ObjectCreate(StrToInteger(label), label, OBJ_TREND, 0, x1, y1, x2, y2, 0, 0);
    ObjectSet(label, OBJPROP_RAY, 0);
    ObjectSet(label, OBJPROP_COLOR, lineColor);
    ObjectSet(label, OBJPROP_STYLE, style);
    ObjectSetInteger(StrToInteger(label),label,OBJPROP_RAY_RIGHT,false);
    //~~~~~~~~~~
}

void startnInitPara(){
   down = 0;
   top = 0;
   trand = 0;
   trand_count = 1;
   hold = 0;
   c_ind = 1;
   lowest = 0;
   highest = 0;
   tp = 0;
   setStopInd = 0;
   abc_t=0;
   cancelAllOrder();
}

bool isNewBar(){
   static datetime New_Time = 0;
   bool New_Bar = false;
   if (New_Time!= Time[0])
      {
      New_Time = Time[0];
      New_Bar = true;
      }
   return(New_Bar);
}

void processPrice(){
   if(!isNewBar()) return;
   
   if(trand == 0){               
      trand = Close[1] > Open[1] ? 1 : 2;

      if(trand == 1){
         top = High[1];
      } else {
         down = Low[1];
      }
      
   } else if(trand == 1){
      if(Close[1] < Open[1]){
         if(trand_count != 2) down = Low[1];
         trand_count++;
         trand = 2;
      }
      
      top = High[1] > top ? High[1] : top;
      
   } else if(trand == 2) {
      if(Close[1] > Open[1]){
         if(trand_count != 2) top = High[1];
         trand_count++;
         trand = 1;
      }
      
      down = down > Low[1] ? Low[1] : down;
      
   }
   
   if(trand_count > 2){
      tmp_int=iLowest(NULL,0,MODE_LOW,currentBarIndex(),1);
      if(tmp_int!=-1) lowest=Low[tmp_int];
      
      tmp_int=iHighest(NULL,0,MODE_HIGH,currentBarIndex(),1);
      if(tmp_int!=-1) highest=High[tmp_int];
      
      Trend_Line("_Trend_Line_" + TimeToString(Time[0]) + "_top", Time[currentBarIndex()],Time[0],top,top,White,STYLE_SOLID);
      Trend_Line("_Trend_Line_" + TimeToString(Time[0]) + "_down", Time[currentBarIndex()],Time[0],down,down,White,STYLE_SOLID);
   }
}

void cancelAllOrder(){
   
   //Update the exchange rates before closing the orders
   RefreshRates();
        
   //Start a loop to scan all the orders
   //The loop starts from the last otherwise it would miss orders
   for(int i=(OrdersTotal()-1);i>=0;i--){
      
      if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES)==false) continue;
      if(OrderMagicNumber()!=MAGICNBR || OrderSymbol()!=Symbol()) continue; 
      
      //Create the required variables
      //Result variable, to check if the operation is successful or not
      bool res=false;
      
      //Allowed Slippage, which is the difference between current price and close price
      int Slippage=3;
      
      //Closing the order using the correct price depending on the type of order
      if(OrderType()==OP_BUY ){
         res=OrderClose(OrderTicket(),OrderLots(),Bid,Slippage,White);
      } else if(OrderType()==OP_SELL){
         res=OrderClose(OrderTicket(),OrderLots(),Ask,Slippage,White);
      } else {
         res=OrderDelete(OrderTicket(),White);
      }
      
      //If there was an error log it
      if(res==false) Print("ERROR - Unable to close the order - ",OrderTicket()," - ",GetLastError());
   }
}

void inMarket(){
   if(in_m == 0){//收穿
   
      if(!isNewBar()) return;
   
      //for buy
      if(Close[1] > top && ((Close[1] - lowest) / Point() < max_loss)){
         
         int ticket=OrderSend(Symbol(),OP_BUY,lot,Ask,3,0,0,"",MAGICNBR);
         if(ticket<0)
           {
            Print("OrderSend failed with error 1#",GetLastError());
           }
         hold = 1;
      }
      
      //for sell
      if(down > Close[1] && ((highest - Close[1]) / Point() < max_loss)){
         
         int ticket=OrderSend(Symbol(),OP_SELL,lot,Bid,3,0,0,"",MAGICNBR);
         if(ticket<0)
           {
            Print("OrderSend failed with error 2#",GetLastError());
           }
         hold = -1;
      }
   } else if(in_m == 1 && setStopInd == 0){  //STOP單
      int ticket=OrderSend(Symbol(),OP_BUYSTOP,lot,top,3,0,0,"",MAGICNBR);
      if(ticket<0)
        {
         Print("OrderSend failed with error 5#",GetLastError());
        }
           
      ticket=OrderSend(Symbol(),OP_SELLSTOP,lot,down,3,0,0,"",MAGICNBR);
      if(ticket<0)
        {
         Print("OrderSend failed with error 3#",GetLastError());
        }
      setStopInd = 1;
   }
   
}