#define MAGICNBR  20200501

input int i_hour = 10;
double lot = 0.01;
input int win = 100;
input int loss = 100;
input int totalBar = 10;

void OnTick(){

   if(isNewBar()){

      if(Hour() == 23){
         cancelAllOrder();
      } else {
         // if(isOrderCommentOnGoing("T1") && !isOrderCommentPadding("D2")){
         //    double takePro3 = NormalizeDouble(Bid - ((loss / 2) * Point()),Digits);
         //    double stoploss3 = NormalizeDouble(Bid + (loss * Point()),Digits);
         //    int ticket3=OrderSend(Symbol(),OP_SELLSTOP,lot*2,Bid,3,stoploss3,takePro3,"D2",MAGICNBR);
         // }
      }

      if(Hour() == i_hour){
         //VLineCreate("_VLineCreate_" + TimeToString(Time[0]));

         double top = findHigh(i_hour - totalBar);
         double down = findLow(i_hour - totalBar);
         //Print("DoubleToString :" + DoubleToString(100 * Point()));
        // Print(TimeToString(Time[0]) + " - [" + DoubleToString(findHigh(i_hour)) + "]" );
        // Print(TimeToString(Time[0]) + " - [" + DoubleToString(findLow(i_hour)) + "]" );

         Trend_Line("_Trend_Line_" + TimeToString(Time[0]) + "_top", Time[i_hour - totalBar],Time[1],top,top,White,STYLE_SOLID);
         Trend_Line("_Trend_Line_" + TimeToString(Time[0]) + "_down", Time[i_hour - totalBar],Time[1],down,down,White,STYLE_SOLID);

         double takePro = NormalizeDouble(top + (win * Point()),Digits);
         double stoploss = NormalizeDouble(top - (loss * Point()),Digits);
         // double takePro = NormalizeDouble(top + (top - down),Digits);
         // double stoploss = down;
         int ticket=OrderSend(Symbol(),OP_BUYSTOP,lot,top,3,stoploss,takePro,"T1",MAGICNBR);
         if(ticket<0){
            Print("OrderSend failed with error 4#",GetLastError());
         }

         double takePro2 = NormalizeDouble(down - (win * Point()),Digits);
         double stoploss2 = NormalizeDouble(down + (loss * Point()),Digits);
         // double takePro2 = NormalizeDouble(down - (top - down),Digits);
         // double stoploss2 = top;
         int ticket2=OrderSend(Symbol(),OP_SELLSTOP,lot,down,3,stoploss2,takePro2,"D1",MAGICNBR);
         if(ticket2<0){
            Print("OrderSend failed with error 3#",GetLastError());
         }
      }
   }
}

double findHigh(int KPeriod){
   return High[iHighest(Symbol(), 0, MODE_HIGH, KPeriod, 1)];
}

double findLow(int KPeriod){
   return Low[iLowest(Symbol(), 0, MODE_LOW, KPeriod, 1)];
}

void Trend_Line(
    string id,
    datetime x1, datetime x2, double y1, 
    double y2, color lineColor, double style){
    //~~~~~~~~~~s
    string label = id;
    ObjectCreate(StrToInteger(label), label, OBJ_TREND, 0, x1, y1, x2, y2, 0, 0);
    ObjectSet(label, OBJPROP_RAY, 0);
    ObjectSet(label, OBJPROP_COLOR, lineColor);
    ObjectSet(label, OBJPROP_STYLE, style);
    ObjectSetInteger(StrToInteger(label),label,OBJPROP_RAY_RIGHT,false);
    //~~~~~~~~~~
}

bool VLineCreate(string            chart_ID=0,        // chart's ID
                  const int             sub_window=0,      // subwindow index
                  datetime              time=0,            // line time
                  const color           clr=White,        // line color
                  const ENUM_LINE_STYLE style=STYLE_SOLID, // line style
                  const int             width=1,           // line width
                  const bool            back=false,        // in the background
                  const bool            selection=true,    // highlight to move
                  const bool            hidden=true,       // hidden in the object list
                  const long            z_order=-10){        // priority for mouse click

   //--- if the line time is not set, draw it via the last bar
   if(!time)
      time=TimeCurrent();
   //--- reset the error value
   ResetLastError();
   //--- create a vertical line

   string label = chart_ID;

   if(!ObjectCreate(StrToInteger(label), label,OBJ_VLINE,sub_window,time,0))
   {
      Print(__FUNCTION__,
         ": failed to create a vertical line! Error code = ",GetLastError());
      return(false);
   }

   ObjectSet(label, OBJPROP_COLOR, clr);

   ObjectSet(label,OBJPROP_ZORDER,z_order);

   return(true);
}

void buy(){
   int ticket=OrderSend(Symbol(),OP_BUY,0.01,Ask,3,0,0,"H1_BUY",MAGICNBR);

   if(ticket<0){
      Print("OrderSend failed with error 1#",GetLastError());
   }   
}

void sell(){
   int ticket=OrderSend(Symbol(),OP_SELL,0.01,Bid,3,0,0,"H1_SELL",MAGICNBR);

   if(ticket<0){
      Print("OrderSend failed with error 2#",GetLastError());
   }   
}


bool isNewBar(){
   static datetime New_Time = 0;
   bool New_Bar = false;
   if (New_Time!= Time[0])
      {
      New_Time = Time[0];
      New_Bar = true;
      }
   return(New_Bar);
}

bool isOrderCommentOnGoing(string comment){
   RefreshRates();

   for(int i=0; i < OrdersTotal(); i++){
      if(OrderSelect(i, SELECT_BY_POS, MODE_TRADES) == true){
         if(OrderComment() == comment && OrderType() <= 1){
            Print("OrderType : " + OrderType());
            return true;
         }
      }
   }

   return false;
}

bool isOrderCommentPadding(string comment){
   RefreshRates();

   for(int i=0; i < OrdersTotal(); i++){
      if(OrderSelect(i, SELECT_BY_POS, MODE_TRADES) == true){
         if(OrderComment() == comment && OrderType() > 1){
            Print("OrderType : " + OrderType());
            return true;
         }
      }
   }

   return false;
}


void cancelAllOrder(){
   
   //Update the exchange rates before closing the orders
   RefreshRates();
        
   //Start a loop to scan all the orders
   //The loop starts from the last otherwise it would miss orders
   for(int i=(OrdersTotal()-1);i>=0;i--){
      
      if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES)==false) continue;
      if(OrderMagicNumber()!=MAGICNBR || OrderSymbol()!=Symbol()) continue; 
      
      //Create the required variables
      //Result variable, to check if the operation is successful or not
      bool res=false;
      
      //Allowed Slippage, which is the difference between current price and close price
      int Slippage=3;
      
      //Closing the order using the correct price depending on the type of order
      if(OrderType()==OP_BUY ){
         res=OrderClose(OrderTicket(),OrderLots(),Bid,Slippage,White);
      } else if(OrderType()==OP_SELL){
         res=OrderClose(OrderTicket(),OrderLots(),Ask,Slippage,White);
      } else {
         res=OrderDelete(OrderTicket(),White);
      }
      
      //If there was an error log it
      if(res==false) Print("ERROR - Unable to close the order - ",OrderTicket()," - ",GetLastError());
   }
}
