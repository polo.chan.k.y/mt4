#define MAGICNBR  20200501

void timer(){

   int thisbarminutes=Period();
   double thisbarseconds=thisbarminutes*60;
   double seconds=thisbarseconds -(TimeCurrent()-Time[0]); // seconds left in bar 

   double minutes= MathFloor(seconds/60);
   double hours  = MathFloor(seconds/3600);

   minutes = minutes -  hours*60;
   seconds = seconds - minutes*60 - hours*3600;


   string sText=DoubleToStr(seconds,0);
   if(StringLen(sText)<2) sText="0"+sText;
   string mText=DoubleToStr(minutes,0);
   if(StringLen(mText)<2) mText="0"+mText;
   string hText=DoubleToStr(hours,0);
   if(StringLen(hText)<2) hText="0"+hText;

   ObjectCreate("Time_Remaining",OBJ_LABEL,0,0,0);
   if(Period()<240) ObjectSetText("Time_Remaining","Time Remaining = "+mText+":"+sText,20,"Arial",White);
   else ObjectSetText("Time_Remaining","Time Remaining = "+hText+":"+mText+":"+sText,20,"Arial",White);
   ObjectSet("Time_Remaining",OBJPROP_CORNER,1);
   ObjectSet("Time_Remaining",OBJPROP_XDISTANCE,0);
   ObjectSet("Time_Remaining",OBJPROP_YDISTANCE,70);
   ObjectSet("Time_Remaining",OBJPROP_BACK,False);

  }
void OnTick(){
   timer();
}

void OnInit(){


   ObjectCreate(_Symbol,"BuyButtonA",OBJ_BUTTON,0,0,0);
   ObjectCreate(_Symbol,"SellButtonB",OBJ_BUTTON,0,0,0);
   ObjectCreate(_Symbol,"BuyButtonC",OBJ_BUTTON,0,0,0);
   ObjectCreate(_Symbol,"SellButtonD",OBJ_BUTTON,0,0,0);
   ObjectCreate(_Symbol,"BuyButtonE",OBJ_BUTTON,0,0,0);
   ObjectCreate(_Symbol,"SellButtonF",OBJ_BUTTON,0,0,0);

   ObjectCreate(_Symbol,"SellButtonA2",OBJ_BUTTON,0,0,0);
   ObjectCreate(_Symbol,"BuyButtonB2",OBJ_BUTTON,0,0,0);
   ObjectCreate(_Symbol,"SellButtonC2",OBJ_BUTTON,0,0,0);
   ObjectCreate(_Symbol,"BuyButtonD2",OBJ_BUTTON,0,0,0);
   ObjectCreate(_Symbol,"SellButtonE2",OBJ_BUTTON,0,0,0);
   ObjectCreate(_Symbol,"BuyButtonF2",OBJ_BUTTON,0,0,0);

   ObjectSetInteger(_Symbol,"BuyButtonA",OBJPROP_XDISTANCE,400);
   ObjectSetInteger(_Symbol,"BuyButtonA",OBJPROP_XSIZE,200);
   ObjectSetInteger(_Symbol,"BuyButtonA",OBJPROP_YDISTANCE,200);
   ObjectSetInteger(_Symbol,"BuyButtonA",OBJPROP_YSIZE,50);
   ObjectSetInteger(_Symbol,"BuyButtonA",OBJPROP_CORNER,3);
   ObjectSetString(_Symbol,"BuyButtonA",OBJPROP_TEXT,"Buy A");

   ObjectSetInteger(_Symbol,"SellButtonB",OBJPROP_XDISTANCE,200);
   ObjectSetInteger(_Symbol,"SellButtonB",OBJPROP_XSIZE,200);
   ObjectSetInteger(_Symbol,"SellButtonB",OBJPROP_YDISTANCE,200);
   ObjectSetInteger(_Symbol,"SellButtonB",OBJPROP_YSIZE,50);
   ObjectSetInteger(_Symbol,"SellButtonB",OBJPROP_CORNER,3);
   ObjectSetString(_Symbol,"SellButtonB",OBJPROP_TEXT,"Sell B");

   ObjectSetInteger(_Symbol,"BuyButtonC",OBJPROP_XDISTANCE,400);
   ObjectSetInteger(_Symbol,"BuyButtonC",OBJPROP_XSIZE,200);
   ObjectSetInteger(_Symbol,"BuyButtonC",OBJPROP_YDISTANCE,150);
   ObjectSetInteger(_Symbol,"BuyButtonC",OBJPROP_YSIZE,50);
   ObjectSetInteger(_Symbol,"BuyButtonC",OBJPROP_CORNER,3);
   ObjectSetString(_Symbol,"BuyButtonC",OBJPROP_TEXT,"Buy C");

   ObjectSetInteger(_Symbol,"SellButtonD",OBJPROP_XDISTANCE,200);
   ObjectSetInteger(_Symbol,"SellButtonD",OBJPROP_XSIZE,200);
   ObjectSetInteger(_Symbol,"SellButtonD",OBJPROP_YDISTANCE,150);
   ObjectSetInteger(_Symbol,"SellButtonD",OBJPROP_YSIZE,50);
   ObjectSetInteger(_Symbol,"SellButtonD",OBJPROP_CORNER,3);
   ObjectSetString(_Symbol,"SellButtonD",OBJPROP_TEXT,"Sell D");

   ObjectSetInteger(_Symbol,"BuyButtonE",OBJPROP_XDISTANCE,400);
   ObjectSetInteger(_Symbol,"BuyButtonE",OBJPROP_XSIZE,200);
   ObjectSetInteger(_Symbol,"BuyButtonE",OBJPROP_YDISTANCE,100);
   ObjectSetInteger(_Symbol,"BuyButtonE",OBJPROP_YSIZE,50);
   ObjectSetInteger(_Symbol,"BuyButtonE",OBJPROP_CORNER,3);
   ObjectSetString(_Symbol,"BuyButtonE",OBJPROP_TEXT,"Buy E");

   ObjectSetInteger(_Symbol,"SellButtonF",OBJPROP_XDISTANCE,200);
   ObjectSetInteger(_Symbol,"SellButtonF",OBJPROP_XSIZE,200);
   ObjectSetInteger(_Symbol,"SellButtonF",OBJPROP_YDISTANCE,100);
   ObjectSetInteger(_Symbol,"SellButtonF",OBJPROP_YSIZE,50);
   ObjectSetInteger(_Symbol,"SellButtonF",OBJPROP_CORNER,3);
   ObjectSetString(_Symbol,"SellButtonF",OBJPROP_TEXT,"Sell F");


   long y_distance = 250;



   ObjectSetInteger(_Symbol,"SellButtonA2",OBJPROP_XDISTANCE,400);
   ObjectSetInteger(_Symbol,"SellButtonA2",OBJPROP_XSIZE,200);
   ObjectSetInteger(_Symbol,"SellButtonA2",OBJPROP_YDISTANCE,200 + y_distance);
   ObjectSetInteger(_Symbol,"SellButtonA2",OBJPROP_YSIZE,50);
   ObjectSetInteger(_Symbol,"SellButtonA2",OBJPROP_CORNER,3);
   ObjectSetString(_Symbol,"SellButtonA2",OBJPROP_TEXT,"Sell A");

   ObjectSetInteger(_Symbol,"BuyButtonB2",OBJPROP_XDISTANCE,200);
   ObjectSetInteger(_Symbol,"BuyButtonB2",OBJPROP_XSIZE,200);
   ObjectSetInteger(_Symbol,"BuyButtonB2",OBJPROP_YDISTANCE,200 + y_distance);
   ObjectSetInteger(_Symbol,"BuyButtonB2",OBJPROP_YSIZE,50);
   ObjectSetInteger(_Symbol,"BuyButtonB2",OBJPROP_CORNER,3);
   ObjectSetString(_Symbol,"BuyButtonB2",OBJPROP_TEXT,"Buy B");

   ObjectSetInteger(_Symbol,"SellButtonC2",OBJPROP_XDISTANCE,400);
   ObjectSetInteger(_Symbol,"SellButtonC2",OBJPROP_XSIZE,200);
   ObjectSetInteger(_Symbol,"SellButtonC2",OBJPROP_YDISTANCE,150 + y_distance);
   ObjectSetInteger(_Symbol,"SellButtonC2",OBJPROP_YSIZE,50);
   ObjectSetInteger(_Symbol,"SellButtonC2",OBJPROP_CORNER,3);
   ObjectSetString(_Symbol,"SellButtonC2",OBJPROP_TEXT,"Sell C");

   ObjectSetInteger(_Symbol,"BuyButtonD2",OBJPROP_XDISTANCE,200);
   ObjectSetInteger(_Symbol,"BuyButtonD2",OBJPROP_XSIZE,200);
   ObjectSetInteger(_Symbol,"BuyButtonD2",OBJPROP_YDISTANCE,150 + y_distance);
   ObjectSetInteger(_Symbol,"BuyButtonD2",OBJPROP_YSIZE,50);
   ObjectSetInteger(_Symbol,"BuyButtonD2",OBJPROP_CORNER,3);
   ObjectSetString(_Symbol,"BuyButtonD2",OBJPROP_TEXT,"Buy D");

   ObjectSetInteger(_Symbol,"SellButtonE2",OBJPROP_XDISTANCE,400);
   ObjectSetInteger(_Symbol,"SellButtonE2",OBJPROP_XSIZE,200);
   ObjectSetInteger(_Symbol,"SellButtonE2",OBJPROP_YDISTANCE,100 + y_distance);
   ObjectSetInteger(_Symbol,"SellButtonE2",OBJPROP_YSIZE,50);
   ObjectSetInteger(_Symbol,"SellButtonE2",OBJPROP_CORNER,3);
   ObjectSetString(_Symbol,"SellButtonE2",OBJPROP_TEXT,"Sell E");

   ObjectSetInteger(_Symbol,"BuyButtonF2",OBJPROP_XDISTANCE,200);
   ObjectSetInteger(_Symbol,"BuyButtonF2",OBJPROP_XSIZE,200);
   ObjectSetInteger(_Symbol,"BuyButtonF2",OBJPROP_YDISTANCE,100 + y_distance);
   ObjectSetInteger(_Symbol,"BuyButtonF2",OBJPROP_YSIZE,50);
   ObjectSetInteger(_Symbol,"BuyButtonF2",OBJPROP_CORNER,3);
   ObjectSetString(_Symbol,"BuyButtonF2",OBJPROP_TEXT,"Buy F");
}


void OnChartEvent(const int id,
                  const long &lparam,
                  const double &dparam,
                  const string &sparam){

   // ObjectCreate(_Symbol,"BuyButtonA",OBJ_BUTTON,0,0,0);
   // ObjectCreate(_Symbol,"SellButtonB",OBJ_BUTTON,0,0,0);
   // ObjectCreate(_Symbol,"BuyButtonC",OBJ_BUTTON,0,0,0);
   // ObjectCreate(_Symbol,"SellButtonD",OBJ_BUTTON,0,0,0);
   // ObjectCreate(_Symbol,"BuyButtonE",OBJ_BUTTON,0,0,0);
   // ObjectCreate(_Symbol,"SellButtonF",OBJ_BUTTON,0,0,0);

   // ObjectCreate(_Symbol,"SellButtonA2",OBJ_BUTTON,0,0,0);
   // ObjectCreate(_Symbol,"BuyButtonB2",OBJ_BUTTON,0,0,0);
   // ObjectCreate(_Symbol,"SellButtonC2",OBJ_BUTTON,0,0,0);
   // ObjectCreate(_Symbol,"BuyButtonD2",OBJ_BUTTON,0,0,0);
   // ObjectCreate(_Symbol,"SellButtonE2",OBJ_BUTTON,0,0,0);
   // ObjectCreate(_Symbol,"BuyButtonF2",OBJ_BUTTON,0,0,0);
   double lot = 0.05;
   int tp = 100;
   int sl = 100;

   if(id==CHARTEVENT_OBJECT_CLICK){

      double bbLow = iBands(NULL,0,7,1.8,0,PRICE_CLOSE,2,1);
      double bbHigh = iBands(NULL,0,7,1.8,0,PRICE_CLOSE,1,1);

      if(sparam == "BuyButtonA"){

         double tp_A = bbHigh;
         double sl_A = Low[1];

         int ticket_A = OrderSend(_Symbol,OP_BUY,lot,Ask,3,sl_A,tp_A,"Polo Buy Order A",0,0,Green);

         if(ticket_A<0)
           {
            Print("OrderSend failed with error [Polo Buy Order A]#",GetLastError());
           }


         double takePro = NormalizeDouble(Low[1] + ((Close[1] - Low[1]) / 2),Digits);

         int ticket_A_B=OrderSend(_Symbol,OP_SELLSTOP,lot*2,Low[1],3,Close[1],takePro,"Polo Buy Order B",0,0,Green);
         if(ticket_A_B<0)
           {
            Print("OrderSend failed with error [Polo Buy Order B]#",GetLastError());
           }

         ObjectSetInteger(_Symbol,"BuyButtonA",OBJPROP_STATE,false);

      }

      if(sparam == "SellButtonB"){

         double tp_B = Low[1]-(20*_Point);
         double sl_B = High[1];

         int ticket_B = OrderSend(_Symbol,OP_SELL,lot,Bid,3,sl_B,tp_B,"Polo Buy Order B",0,0,Green);

         if(ticket_B<0)
           {
            Print("OrderSend failed with error 2#",GetLastError());
           }

           ObjectSetInteger(_Symbol,"SellButtonB",OBJPROP_STATE,false);

      }

      if(sparam == "BuyButtonC"){

         double tp_C = High[1]+(20*_Point);
         double sl_C = Low[1];

         int ticket_C = OrderSend(_Symbol,OP_BUY,lot,Ask,3,sl_C,tp_C,"Polo Buy Order C",0,0,Green);

         if(ticket_C<0)
           {
            Print("OrderSend failed with error 1#",GetLastError());
           }

         ObjectSetInteger(_Symbol,"BuyButtonC",OBJPROP_STATE,false);
      }
      


      if(sparam == "SellButtonA2"){

         double tp_A2 = bbLow;
         double sl_A2 = High[1];

         int ticket_A2 = OrderSend(_Symbol,OP_SELL,lot,Ask,3,sl_A2,tp_A2,"Polo Sell Order A",0,0,Green);

         if(ticket_A2<0)
           {
            Print("OrderSend failed with error [Polo Sell Order A]#",GetLastError());
           }


         double takePro_A2 = NormalizeDouble(High[1] + ((High[1] - Close[1]) / 2),Digits);

         int ticket_A_B2=OrderSend(_Symbol,OP_BUYSTOP,lot*2,High[1],3,Close[1],takePro_A2,"Polo Buy Order B",0,0,Green);
         
         if(ticket_A_B2<0)
           {
            Print("OrderSend failed with error [Polo Buy Order B]#",GetLastError());
           }

         ObjectSetInteger(_Symbol,"SellButtonA2",OBJPROP_STATE,false);
      }
   }
}